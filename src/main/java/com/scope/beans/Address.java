package com.scope.beans;

public class Address {
    private String city;

    public Address() {
        System.out.println("Address Constructor Invoked");
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
