package com.scope;

import com.scope.beans.Address;
import com.scope.beans.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestScope {
    public static void main(String[] args) {
        System.out.println("main() method");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        System.out.println("application context initialized");
        Student s1 = (Student) context.getBean("student");
        Student s2 = (Student) context.getBean("student");
        Address a1 = (Address) context.getBean("address");
        Address a2 = (Address) context.getBean("address");
    }
}
