package com.scope.config;

import com.scope.beans.Address;
import com.scope.beans.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class BeanCreationFactoryConfig {

    @Bean(name = "student")
    public Student getStudent() {
        return new Student();
    }

    @Bean(name = "address")
    @Scope("prototype")
    public Address getAddress() {
        return new Address();
    }
}
