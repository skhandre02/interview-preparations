1. Created a Student class with attribute as name(String), address(Address)
2. Created a Address class with attribute as city(String)
3. BeanCreationFactoryConfig is used to create Beans. Bean for Student class is created with scope as Singleton and Address class is created with scope as Prototype.
4. TestScope class is a main class 